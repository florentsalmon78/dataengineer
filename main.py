import mysql.connector
from mysql.connector import Error
import requests

def check_arrondissement(city):
    '''
    Fonction pour enlever les arrondissements sur le nom d'une ville
    '''
    if ' ' in city:
        city_split = city.split(' ')
        if city_split[1][0].isnumeric():
            return city_split[0]
        else:
            return city
    else:
        return city


def fix_postalcode(code):
    '''
    Fonction pour fixer les codes postaux commençant par un 0 
    '''
    if len(code) == 4:
        return f'0{code}'
    else:
        return code

def connection(database):
    '''
    Connexion à la bdd
    '''
    connection = None
    try:
        connection = mysql.connector.connect(
            host="localhost",
            port=8889,
            user="root",
            password="root",
            database=database
        )
        print("MySQL Database connection successful")
    except Error as err:
        print(f"Error: '{err}'")
    return connection


def main():
    mybdd = connection(database='dataengineer')
    cursor = mybdd.cursor()

    # Création des colonnes
    query = "ALTER TABLE address \
                ADD latitude VARCHAR(100), ADD longitude VARCHAR(100)"
    cursor.execute(query)

    # Récupération des adresses
    query = "SELECT * FROM address"
    cursor.execute(query)
    records = cursor.fetchall()

    for row in records:
        id = row[0]
        address = row[1]
        city = check_arrondissement(row[2])
        postal_code = fix_postalcode(row[3])

        # API Nominatim Openstreetmap
        param = f'street={address}&city={city}&postalcode={postal_code}&country=FRANCE'
        response = requests.get(f'https://nominatim.openstreetmap.org/search.php?{param}&format=jsonv2')
        response_json = response.json()

        if response_json:
            lat = response_json[0]['lat']
            lon = response_json[0]['lon']

            # Upgrade colonnes avec latitude et longitude 
            query = f'UPDATE address \
                SET latitude = {lat}, longitude = {lon} \
                where address_id = {id}'
            cursor.execute(query)
            mybdd.commit()
            print(f'{id}')

if __name__ == "__main__":
    main()