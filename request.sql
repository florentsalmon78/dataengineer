SELECT C.last_name, C.first_name, P.customer_id, COUNT(*), A.address, A.city, A.postal_code, A.latitude, A.longitude 
from payment as P
INNER JOIN customer as C ON C.customer_id = P.customer_id
INNER JOIN address as A ON A.address_id = C.address_id
GROUP BY customer_id  
ORDER BY COUNT(*) DESC LIMIT 1;